const express = require('express');

const app = express();
// Middleware pour logger lerequêt
app.use((req, res, next) => {
 console.log(`${req.method} ${req.url}`);
 next();
});
// Middleware pour gérer les erreurs
app.use((err, req, res, next) => {
 console.error(err.stack);
 res.status(500).send('Erreur interne du serveur');
});
// Route principale
app.get('/', (req, res) => {
 res.send('Bienvenue dans notre application Node.js avec middlewares!');
});
// Ecouter sur le port 3000
app.listen(3000, () => {
 console.log('Serveur démarré sur le port 3000');
});